﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using Bank.Api.Models.Workers.Exceptions;
using BankApi.Models.Workers;
using EFxceptions.Models.Exceptions;
using FluentAssertions;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Client.Platforms.Features.DesktopOs.Kerberos;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Bank.Api.Tests.Unit.Services.Workers
{
    public partial class WorkerServiceTests
    {
        [Fact]
        public async Task ShouldThrowCriticalDependencyExceptionOnAddIfSqlErrorOccursAndLogItAsync()
        {
            //given
            Worker someWorker = CreateRandomWorker();
            SqlException sqlException = CreateSqlException();

            var failedWorkerStorageException = new FailedWorkerStorageException(sqlException);
            var expectedWorkerDependencyException =
                new WorkerDependencyException(failedWorkerStorageException);

            this.storageBrokerMock.Setup(broker =>
                broker.InsertWorkerAsync(It.IsAny<Worker>())).ThrowsAsync(sqlException);

            //when
            ValueTask<Worker> addWorkerTask = this.workerService.AddWorkerAsync(someWorker);

            WorkerDependencyException actualDependencyException =
                await Assert.ThrowsAsync<WorkerDependencyException>(addWorkerTask.AsTask);

            //then
            actualDependencyException.Should().BeEquivalentTo(expectedWorkerDependencyException);

            this.storageBrokerMock.Verify(broker =>
                broker.InsertWorkerAsync(It.IsAny<Worker>()), Times.Once);

            this.loggingBrokerMock.Verify(broker =>
                broker.LogCritical(It.Is(SameExceptionAs(
                    expectedWorkerDependencyException))), Times.Once);

            this.storageBrokerMock.VerifyNoOtherCalls();
            this.loggingBrokerMock.VerifyNoOtherCalls();
        }

        [Fact]
        public async Task ShouldThrowDependencyExceptionOnAddIfDuplicateKeyErrorOccursAndLogItAsync()
        {
            //given
            Worker someWorker = CreateRandomWorker();
            string someMessage = CreateRandomString();
            var duplicateKeyException = new DuplicateKeyException(someMessage);

            var alreadyExistWorkerException =
                new AlreadyExistWorkerException(duplicateKeyException);

            var expectedWorkerDependencyValidationException =
                new WorkerDependencyValidationException(alreadyExistWorkerException);

            this.storageBrokerMock.Setup(broker => broker.InsertWorkerAsync(It.IsAny<Worker>()))
                .ThrowsAsync(duplicateKeyException);

            //when
            ValueTask<Worker> addWorkerTask = this.workerService.AddWorkerAsync(someWorker);

            WorkerDependencyValidationException actualWorkerDependencyValidationException =
                await Assert.ThrowsAsync<WorkerDependencyValidationException>(addWorkerTask.AsTask);

            //then
            actualWorkerDependencyValidationException.Should().BeEquivalentTo(
                expectedWorkerDependencyValidationException);          

            this.storageBrokerMock.Verify(broker => 
                broker.InsertWorkerAsync(It.IsAny<Worker>()), Times.Once);

            this.loggingBrokerMock.Verify(broker => broker.LogError(It.Is(
                SameExceptionAs(expectedWorkerDependencyValidationException))), Times.Once);

            this.storageBrokerMock.VerifyNoOtherCalls();
            this.loggingBrokerMock.VerifyNoOtherCalls();
        }

        [Fact]
        public async Task ShouldThrowDependencyValidationExceptionOnAddIfDbConcurrencyErrorOccursAndLogItAsync()
        {
            //given
            Worker someWorker = CreateRandomWorker();
            var dbUpdateConcurrencyException = new DbUpdateConcurrencyException();

            var lockedWorkerException = new LockedWorkerException(dbUpdateConcurrencyException);

            var expectedWorkerDependencyValidationException = 
                new WorkerDependencyValidationException(lockedWorkerException);

            this.storageBrokerMock.Setup(broker => broker.InsertWorkerAsync(It.IsAny<Worker>()))
                .ThrowsAsync(dbUpdateConcurrencyException);

            //when
            ValueTask<Worker> addWorkerTask = this.workerService.AddWorkerAsync(someWorker);

            WorkerDependencyValidationException actualWorkerDependencyValidationException =
                await Assert.ThrowsAsync<WorkerDependencyValidationException>(addWorkerTask.AsTask);

            //then
            actualWorkerDependencyValidationException.Should().BeEquivalentTo(expectedWorkerDependencyValidationException);

            this.storageBrokerMock.Verify(broker => broker.InsertWorkerAsync(It.IsAny<Worker>()), Times.Once);

            this.loggingBrokerMock.Verify(broker => broker.LogError(It.Is(
                SameExceptionAs(expectedWorkerDependencyValidationException))), Times.Once);

            this.storageBrokerMock.VerifyNoOtherCalls();
            this.loggingBrokerMock.VerifyNoOtherCalls();

        }

        [Fact]
        public async Task ShouldThrowServiceValidationExceptionOnAddIfServiceErrorOccursAndLogItAsync()
        {
            //given 
            Worker someWorker = CreateRandomWorker();
            var serviceException = new Exception();

            FailedWorkerServiceException failedWorkerServiceException = 
                new FailedWorkerServiceException(serviceException);

            var expectedWorkerServiceException = new WorkerServiceException(failedWorkerServiceException);

            this.storageBrokerMock.Setup(broker => broker.InsertWorkerAsync(It.IsAny<Worker>()))
                .ThrowsAsync(expectedWorkerServiceException);

            //when
            ValueTask<Worker> addWorkerTask = this.workerService.AddWorkerAsync(someWorker);

            WorkerServiceException actualWorkerServiceException = await Assert
                .ThrowsAsync<WorkerServiceException>(addWorkerTask.AsTask);

            //then
            actualWorkerServiceException.Should().BeEquivalentTo(expectedWorkerServiceException);

            this.storageBrokerMock.Verify(broker => 
                broker.InsertWorkerAsync(It.IsAny<Worker>()), Times.Once);

            this.loggingBrokerMock.Verify(broker => broker.LogError(It.Is(
                SameExceptionAs(expectedWorkerServiceException))), Times.Once);

            this.storageBrokerMock.VerifyNoOtherCalls();
            this.loggingBrokerMock.VerifyNoOtherCalls();
        }
    }
}
