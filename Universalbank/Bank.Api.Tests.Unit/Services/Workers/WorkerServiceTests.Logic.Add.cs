﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using BankApi.Models.Workers;
using FluentAssertions;
using Force.DeepCloner;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Bank.Api.Tests.Unit.Services.Workers
{
    public partial class WorkerServiceTests
    {
        [Fact]
        
        public async Task ShouldAddWorkerAsync()
        {
            //given
            Worker randomWorker = CreateRandomWorker();
            Worker inputWorker = randomWorker;
            Worker storedWorker = inputWorker;
            Worker expectedWorker = storedWorker.DeepClone();

            this.storageBrokerMock.Setup(broker =>
               broker.InsertWorkerAsync(inputWorker)).ReturnsAsync(storedWorker);

            //when
            Worker actualWorker = await this.workerService.AddWorkerAsync(inputWorker);

            //then
            actualWorker.Should().BeEquivalentTo(expectedWorker);

            this.storageBrokerMock.Verify(broker =>
            broker.InsertWorkerAsync(inputWorker), Times.Once());

            this.loggingBrokerMock.VerifyNoOtherCalls();
            this.storageBrokerMock.VerifyNoOtherCalls();


        }
    }
}
