﻿using Bank.Api.Models.Workers.Exceptions;
using BankApi.Models.Workers;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Bank.Api.Tests.Unit.Services.Workers
{
    public partial class WorkerServiceTests
    {
        [Fact]

        public async Task ShouldThrowValidationExceptionOnAddIfInputIsNullAndLogItAsync()
        {
            //given
            Worker noWorker = null;
            var nullWorkerException = new NullWorkerException();

            var expectedWorkerValidationException =
                new WorkerValidationException(nullWorkerException);

            //when
            ValueTask<Worker> addWorkerTask =
                this.workerService.AddWorkerAsync(noWorker);

            WorkerValidationException actualWorkerValidationException =
                await Assert.ThrowsAsync<WorkerValidationException>(addWorkerTask.AsTask);

            //then
            actualWorkerValidationException.Should().BeEquivalentTo(expectedWorkerValidationException);

            this.loggingBrokerMock.Verify(broker =>
            broker.LogError(It.Is(SameExceptionAs(expectedWorkerValidationException))), Times.Once);

            this.storageBrokerMock.Verify(broker =>
            broker.InsertWorkerAsync(It.IsAny<Worker>()), Times.Never);

            this.loggingBrokerMock.VerifyNoOtherCalls();
            this.storageBrokerMock.VerifyNoOtherCalls();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public async Task ShouldThrowValidationExceptionOnAddIfWorkerIsInvalidAndLogItAsync(
            string invalidString)
        {
            //given
            var invalidWorker = new Worker
            {
                Title = invalidString
            };

            var invalidWorkerException = new InvalidWorkerException();

            invalidWorkerException.AddData(
                key: nameof(Worker.Id),
                values: "Id is required");

            invalidWorkerException.AddData(
                key: nameof(Worker.FirstName),
                values: "Text is required");

            invalidWorkerException.AddData(
                key: nameof(Worker.LastName),
                values: "Text is required");

            invalidWorkerException.AddData(
                key: nameof(Worker.Title),
                values: "Text is required");

            invalidWorkerException.AddData(
                key: nameof(Worker.Email),
                values: "Text is required");

            invalidWorkerException.AddData(
                key: nameof(Worker.Password),
                values: "Text is required");

            invalidWorkerException.AddData(
                key: nameof(Worker.CreatedDate),
                values: "Value is required");

            invalidWorkerException.AddData(
                key: nameof(Worker.UpdatedDate),
                values: "Value is required");

            var expectedWorkerException =
                new WorkerValidationException(invalidWorkerException);

            //when
            ValueTask<Worker> addWorkerTask = this.workerService.AddWorkerAsync(invalidWorker);

            WorkerValidationException actualWorkerValidationException =
                await Assert.ThrowsAsync<WorkerValidationException>(addWorkerTask.AsTask);

            //then
            actualWorkerValidationException.Should().BeEquivalentTo(expectedWorkerException);

            this.loggingBrokerMock.Verify(broker =>
                broker.LogError(It.Is(SameExceptionAs(expectedWorkerException))), Times.Once);

            this.storageBrokerMock.Verify(broker =>
                broker.InsertWorkerAsync(It.IsAny<Worker>()), Times.Never);

            this.loggingBrokerMock.VerifyNoOtherCalls();
            this.storageBrokerMock.VerifyNoOtherCalls();
        }

        [Fact]
        public async Task ShouldThrowValidationExceptionOnAddIfCreatedDateIsNotSameAsUpdatedDateAndLogItAsync()
        {
            //given
            DateTimeOffset anotherRandomDate = GetRandomDateTime();
            Worker randomWorker = CreateRandomWorker();
            Worker invalidWorker = randomWorker;
            invalidWorker.UpdatedDate = anotherRandomDate;
            var invalidWorkerException = new InvalidWorkerException();

            invalidWorkerException.AddData(
                key: nameof(Worker.CreatedDate),
                values: $"Date is not same as {nameof(Worker.UpdatedDate)}");

            var expectedWorkerValidationException =
                new WorkerValidationException(invalidWorkerException);

            //when
            ValueTask<Worker> addWorkerTask = this.workerService
                .AddWorkerAsync(invalidWorker);

            WorkerValidationException actualWorkerValidationException =
                await Assert.ThrowsAsync<WorkerValidationException>(addWorkerTask.AsTask);

            //then
            actualWorkerValidationException.Should().BeEquivalentTo(expectedWorkerValidationException);

            this.loggingBrokerMock.Verify(broker => broker.LogError(
                It.Is(SameExceptionAs(expectedWorkerValidationException))));

            this.storageBrokerMock.Verify(broker => broker.InsertWorkerAsync(
                It.IsAny<Worker>()), Times.Never);

            this.loggingBrokerMock.VerifyNoOtherCalls();
            this.storageBrokerMock.VerifyNoOtherCalls();
        }
    }
}
