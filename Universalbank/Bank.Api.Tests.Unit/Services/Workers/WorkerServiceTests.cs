﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using Bank.Api.Brokers.Loggings;
using Bank.Api.Brokers.Storages;
using Bank.Api.Services.Workers;
using BankApi.Models.Workers;
using Microsoft.Data.SqlClient;
using Moq;
using System;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using Tynamix.ObjectFiller;
using Xeptions;

namespace Bank.Api.Tests.Unit.Services.Workers
{
    public partial class WorkerServiceTests
    {
        private readonly Mock<IStorageBroker> storageBrokerMock;
        private readonly Mock<ILoggingBroker> loggingBrokerMock;
        private readonly IWorkerService workerService;

        public WorkerServiceTests()
        {
            this.storageBrokerMock = new Mock<IStorageBroker>();
            this.loggingBrokerMock = new Mock<ILoggingBroker>();

            this.workerService = new WorkerService(
                storageBroker: this.storageBrokerMock.Object,
                loggingBroker: this.loggingBrokerMock.Object);
        }

        private Expression<Func<Xeption, bool>> SameExceptionAs(Xeption exceptedException) =>
            actualException => actualException.SameExceptionAs(exceptedException);

        private static DateTimeOffset GetRandomDateTime() =>
            new DateTimeRange(earliestDate: DateTime.UnixEpoch).GetValue();

        private static SqlException CreateSqlException() =>
            (SqlException)FormatterServices.GetUninitializedObject(typeof(SqlException));

        private static string CreateRandomString() =>
            new MnemonicString().GetValue();

        private static Worker CreateRandomWorker() =>
            CreateWorkerFiller().Create();

        private static Filler<Worker> CreateWorkerFiller()
        {
            var filler = new Filler<Worker>();
            DateTimeOffset dates = GetRandomDateTime();

            filler.Setup()
                .OnType<DateTimeOffset>().Use(dates);

            return filler;

        }
    }
}
