﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using BankApi.Models.Workers;
using System.Threading.Tasks;

namespace Bank.Api.Services.Workers
{
    public interface IWorkerService
    {
        ValueTask<Worker> AddWorkerAsync(Worker worker);
    }
}