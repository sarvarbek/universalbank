﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using Bank.Api.Brokers.Loggings;
using Bank.Api.Brokers.Storages;
using Bank.Api.Models.Workers.Exceptions;
using BankApi.Models.Workers;
using System.Threading.Tasks;

namespace Bank.Api.Services.Workers
{
    public partial class WorkerService : IWorkerService
    {
        private readonly IStorageBroker storageBroker;
        private readonly ILoggingBroker loggingBroker;

        public WorkerService(IStorageBroker storageBroker, ILoggingBroker loggingBroker)
        {
            this.storageBroker = storageBroker;
            this.loggingBroker = loggingBroker;
        }

        public ValueTask<Worker> AddWorkerAsync(Worker worker) =>        
            TryCatch(async () =>
            {
                ValidateWorker(worker);

                return await this.storageBroker.InsertWorkerAsync(worker);
            });
        
            
    }
}
