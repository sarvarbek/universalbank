﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using Bank.Api.Models.Workers.Exceptions;
using BankApi.Models.Workers;
using EFxceptions.Models.Exceptions;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Xeptions;

namespace Bank.Api.Services.Workers
{
    public partial class WorkerService
    {
        private delegate ValueTask<Worker> ReturningWorkerFunction();

        private async ValueTask<Worker> TryCatch(ReturningWorkerFunction returningWorkerFunction)
        {
            try
            {
                return await returningWorkerFunction();
            }
            catch (NullWorkerException nullWorkerException)
            {
                throw CreateAndLogValidationException(nullWorkerException);
            }
            catch (InvalidWorkerException invalidWorkerException)
            {
                throw CreateAndLogValidationException(invalidWorkerException);
            }
            catch (SqlException sqlException)
            {
                var failedWorkerStorageException = new FailedWorkerStorageException(sqlException);

                throw CreateAndLogCriticalDependencyException(failedWorkerStorageException);
            }
            catch (DuplicateKeyException duplicateKeyException)
            {
                var failedWorkerDependencyValidationException =
                    new AlreadyExistWorkerException(duplicateKeyException);

                throw CreateAndLogDependencyValidationExeption(failedWorkerDependencyValidationException);
            }
            catch (DbUpdateConcurrencyException dbUpdateConcurrencyException)
            {
                var lockedWorkerException = new LockedWorkerException(dbUpdateConcurrencyException);

                throw CreateAndLogDependencyValidationExeption(lockedWorkerException);
            }
            catch (Exception serviceException)
            {
                var failedWorkerServiceException = new FailedWorkerServiceException(serviceException);

                throw CreateAndLogServiceException(failedWorkerServiceException);
            }
        }

        private WorkerValidationException CreateAndLogValidationException(Xeption exception)
        {
            var workerValidationException = new WorkerValidationException(exception);
            this.loggingBroker.LogError(workerValidationException);

            throw workerValidationException;
        }
        private WorkerDependencyException CreateAndLogCriticalDependencyException(Xeption exception)
        {
            var workerDependencyException = new WorkerDependencyException(exception);
            this.loggingBroker.LogCritical(workerDependencyException);

            return workerDependencyException;
        }

        private WorkerDependencyValidationException CreateAndLogDependencyValidationExeption(Xeption exception)
        {
            var workerDependencyValidationException = new WorkerDependencyValidationException(exception);
            this.loggingBroker.LogError(workerDependencyValidationException);

            return workerDependencyValidationException;
        }

        private WorkerServiceException CreateAndLogServiceException(Exception exception)
        {
            var workerServiceException= new WorkerServiceException(exception);
            this.loggingBroker.LogError(workerServiceException); 
            
            return workerServiceException;
        }
    }
}