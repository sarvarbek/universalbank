﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using Bank.Api.Models.Workers.Exceptions;
using BankApi.Models.Workers;
using System;

namespace Bank.Api.Services.Workers
{
    public partial class WorkerService
    {
        private static void ValidateWorker(Worker worker)
        {
            ValidateWorkerNotNull(worker);

            Validate(
                (Rule: IsInvalid(worker.Id), Parameter: nameof(Worker.Id)),
                (Rule: IsInvalid(worker.FirstName), Parameter: nameof(Worker.FirstName)),
                (Rule: IsInvalid(worker.LastName), Parameter: nameof(Worker.LastName)),
                (Rule: IsInvalid(worker.Title), Parameter: nameof(Worker.Title)),
                (Rule: IsInvalid(worker.Email), Parameter: nameof(Worker.Email)),
                (Rule: IsInvalid(worker.Password), Parameter: nameof(Worker.Password)),
                (Rule: IsInvalid(worker.CreatedDate), Parameter: nameof(Worker.CreatedDate)),
                (Rule: IsInvalid(worker.UpdatedDate), Parameter: nameof(Worker.UpdatedDate)),

                (Rule: IsNotSame(
                    firstDate: worker.CreatedDate,
                    secondDate: worker.UpdatedDate,
                    secondDateName: nameof(Worker.UpdatedDate)),

                Parameter: nameof(Worker.CreatedDate)));
        }

        private static dynamic IsInvalid(Guid id) => new
        {
            Condition = id == default,
            Message = "Id is required"
        };

        private static dynamic IsInvalid(string text) => new
        {
            Condition = string.IsNullOrWhiteSpace(text),
            Message = "Text is required"
        };

        private static dynamic IsInvalid(DateTimeOffset date) => new
        {
            Condition = date == default,
            Message = "Value is required"
        };

        private static dynamic IsNotSame(
            DateTimeOffset firstDate,
            DateTimeOffset secondDate,
            string secondDateName) => new
        {
            Condition = firstDate != secondDate,
            Message = $"Date is not same as {secondDateName}"
        };

        public static void ValidateWorkerNotNull(Worker worker)
        {
            if (worker is null)
            {
                throw new NullWorkerException();
            }
        }

        private static void Validate(params (dynamic Rule, string Parameter)[] validations)
        {
            var invalidWorkerException = new InvalidWorkerException();

            foreach ((dynamic rule, string parameter) in validations)
            {
                if (rule.Condition)
                {
                    invalidWorkerException.UpsertDataList(
                        key: parameter,
                        value: rule.Message);
                }
            }

            invalidWorkerException.ThrowIfContainsErrors();
        }
    }
}
