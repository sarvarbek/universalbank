﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using BankApi.Models.Workers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Bank.Api.Brokers.Storages
{
    public partial interface IStorageBroker
    {
        ValueTask<Worker> InsertWorkerAsync(Worker worker);
        IQueryable<Worker> SelectAllWorkers();
        ValueTask<Worker> SelectWorkerById(Guid id);
        ValueTask<Worker> UpdateWorkerAsync(Worker worker);
        ValueTask<Worker> DeleteWorkerAsync(Worker worker);
    }
}
