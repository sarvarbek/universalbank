﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using BankApi.Models.Workers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Bank.Api.Brokers.Storages
{
    public partial class StorageBroker
    {
        public DbSet<Worker> Workers { get; set; }

        public async ValueTask<Worker> InsertWorkerAsync(Worker worker) =>
            await InsertAsync(worker);

        public IQueryable<Worker> SelectAllWorkers() =>
            SelectAll<Worker>();

        public async ValueTask<Worker> SelectWorkerById(Guid id) =>
            await SelectAsync<Worker>(id);

        public async ValueTask<Worker> UpdateWorkerAsync(Worker worker) =>
            await UpdateAsync(worker);

        public async ValueTask<Worker> DeleteWorkerAsync(Worker worker) =>
            await DeleteAsync(worker);
    }
}
