﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using System;

namespace Bank.Api.Brokers.Loggings
{
    public interface ILoggingBroker
    {
        void LogCritical(Exception exception);
        void LogError(Exception exception);
    }
}