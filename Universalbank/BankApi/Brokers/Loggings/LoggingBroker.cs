﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using Microsoft.Extensions.Logging;
using System;

namespace Bank.Api.Brokers.Loggings
{
    public class LoggingBroker : ILoggingBroker
    {
        private ILogger<ILoggingBroker> logger;

        public LoggingBroker(ILogger<ILoggingBroker> logger) =>
            this.logger = logger;

        public void LogCritical(Exception exception) =>
            this.logger.LogCritical(exception, exception.Message);
        
        public void LogError(Exception exception) =>
            this.logger.LogError(exception.Message,exception);        
    }
}
