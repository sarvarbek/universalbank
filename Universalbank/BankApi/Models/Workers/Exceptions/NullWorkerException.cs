﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using Xeptions;

namespace Bank.Api.Models.Workers.Exceptions
{
    public class NullWorkerException: Xeption
    {
        public NullWorkerException()
            : base(message: "Worker is null.")
        {}
    }
}
