﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using System;
using Xeptions;

namespace Bank.Api.Models.Workers.Exceptions
{
    public class LockedWorkerException: Xeption
    {
        public LockedWorkerException(Exception innerException)
            : base(message: "Worker is locked, please try again", innerException)
        {}
    }
}
