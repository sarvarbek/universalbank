﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using Xeptions;

namespace Bank.Api.Models.Workers.Exceptions
{
    public class WorkerDependencyException : Xeption
    {
        public WorkerDependencyException(Xeption innerException)
            : base(message: "Worker dependency error occured", innerException) {}
    }
}
