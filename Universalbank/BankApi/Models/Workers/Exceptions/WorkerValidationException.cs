﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using Xeptions;

namespace Bank.Api.Models.Workers.Exceptions
{
    public class WorkerValidationException: Xeption
    {
        public WorkerValidationException(Xeption innerException)
            : base(message: "Worker validation error occured, fix the errors and try again.",
                  innerException)
        {}
    }
}
