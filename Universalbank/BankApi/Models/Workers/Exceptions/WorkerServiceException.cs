﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using System;
using Xeptions;

namespace Bank.Api.Models.Workers.Exceptions
{
    public class WorkerServiceException: Xeption
    {
        public WorkerServiceException(Exception innerException)
            : base(message: "Worker service error occured", innerException)
        {}
    }
}
