﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using Xeptions;

namespace Bank.Api.Models.Workers.Exceptions
{
    public class WorkerDependencyValidationException: Xeption
    {
        public WorkerDependencyValidationException(Xeption innerException)
            : base(message: "Worker dependency validation error occured, fix and try again", innerException)
        {}
    }
}
