﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using System;
using Xeptions;

namespace Bank.Api.Models.Workers.Exceptions
{
    public class FailedWorkerServiceException: Xeption
    {
        public FailedWorkerServiceException(Exception innerException)
            : base(message: "Failed worker service occured", innerException)
        {}
    }
}
