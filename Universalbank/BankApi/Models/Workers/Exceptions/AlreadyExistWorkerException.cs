﻿//==============================================
// Copyright (c) Sarvarbek .NET Developer
// Don't stop moving. I want to be Developer
//==============================================

using System;
using Xeptions;

namespace Bank.Api.Models.Workers.Exceptions
{
    public class AlreadyExistWorkerException: Xeption
    {
        public AlreadyExistWorkerException(Exception innerException)
            : base(message: "Worker already exist", innerException)
        { }
    }
}
